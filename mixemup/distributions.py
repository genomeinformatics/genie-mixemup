from   abc import ABCMeta, abstractmethod
import numpy as np
from   numpy import log, log1p, sum, exp
import collections

LOGPI = log(np.pi)
LOGTWOPI = log(2*np.pi)

#################################

class Distribution(metaclass=ABCMeta):
    """Abstract class for probability distributions.
    Each distribution needs to define a few functions usable by EM."""

    @abstractmethod
    def __init__(self):
        """create a new distribution object"""
        pass

    @abstractmethod
    def __repr__(self):
        return "<abstract Distribution object>"

    @abstractmethod
    def single_loglikelihood(self, x):
        """return the log-likelihood of a single datum x"""
        return None

    @abstractmethod
    def loglikelihoods(self, x, out=None):
        """compute log-likelihoods for data points x.
        If given, fill vector 'out' with log-likelihoods and return it;
        otherwise create a new vector 'out', fill it and return it.
        This abstract slow generic implementation should be replaced with
        vectorized code in each concrete class."""
        if out is None: out = np.ndarray((len(x),))
        ll = self.single_loglikelihood
        for (i,xx) in enumerate(x):
            out[i] = ll(x)
        return out

    @abstractmethod
    def estimate_parameters(self, x, weights):
        """estimate and set new parameters"""
        pass
    
    @abstractmethod
    def initialize_parameters(self, x, weights=None, seed=None):
        """initialize parameters from data 'x' with optionally given weights"""
        pass

    _methods = frozenset(("single_loglikelihood", "loglikelihoods",
                         "estimate_parameters", "initialize_parameters"))
                          
    @property
    def parameters(self):
        d = dict()
        selfd, methods = self.__dict__, self._methods
        for name in selfd.keys():
            if name.startswith("_"): continue
            if name in methods: continue
            d[name] = selfd[name]
        return d


#################################

class FiniteUniformDistribution(Distribution):
    """uniform distribution on an aribtrary given set of values"""
    
    def __init__(self, values):
        """create a new uniform distribution on value set 'values'"""
        self.values = frozenset(values)
        self.logp   = -log(len(self.values))

    @property
    def p(self):
        return exp(self.logp)
    
    def single_loglikelihood(self, x):
        return self.logp if x in self.values else np.NINF
        
    def loglikelihoods(self, x, out=None):
        if out is None: out = np.ndarray((len(x),))
        v, logp, ninf = self.values, self.logp, np.NINF
        f = lambda y: logp if y in v else ninf
        nf = np.frompyfunc(f, 1, 1)  # will not work if x has 2+ dimensions.
        nf(x,out)
        return out

    def estimate_parameters(self, x, weights):
        pass

    def initialize_parameters(self, x, weights=None, seed=None):
        pass

    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, self.values)


#################################

class FiniteDistribution(Distribution):
    """a distribution on an arbitrary finite set of values with parameters"""
    
    def __init__(self, p_by_value, logarithmic=False):
        """create a new finite distribution from a dict or set of values.
        A dict specifies the probability for each value,
        or log-probability if logarithmic==True.
        If only a set is given, the distribution is initialized uniformly."""
        self.values = frozenset(p_by_value)
        if isinstance(p_by_value, dict):
            if logarithmic:
                self.logp = p_by_value
            else:
                self.logp = {v: log(p)  for (v,p) in p_by_vlaue.items()}
        else:
            x = -log(len(self.values))
            self.logp = {v: x  for v in self.values}

    @property
    def p(self):
        return {k: exp(v) for (k,v) in self.logp.items()}

    def single_loglikelihood(self, x):
        return self.logp.get(x, np.NINF)

    def loglikelihoods(self, x, out=None):
        if out is None: out = np.ndarray((len(x),))
        logp, ninf = self.logp, np.NINF
        f = lambda y: logp.get(y, ninf)
        nf = np.frompyfunc(f, 1, 1)  # will not work if x has 2+ dimensions.
        nf(x,out)
        return out
    
    def estimate_parameters(self, x, weights):
        p = collections.defaultdict(lambda: 0.0)
        for (xx, w) in zip(x, weights): p[xx] += w
        s = sum(weights)
        for (xx, pp) in p.items():  p[xx] = log(pp/s)
        self.logp = p

    def initialize_parameters(self, x, weights=None, seed=None):
        if weights is None: weights = np.ones(len(x))
        self.estimate_parameters(x, weights)

    def __repr__(self):
        return "{}({})".format(self.__class__.__name__, repr(self.p))


#################################################


class UnivariateNormalDistribution(Distribution):
    """Gaussian distribution in one dimension"""

    def __init__(self, mean=0.0, variance=1.0):
        """create a new normal distribution with given mean and variance"""
        self.mean = mean
        self.variance = variance

    def __repr__(self):
        return "{}(mean={:g}, variance={:g})".format(
            self.__class__.__name__, self.mean, self.variance)

    def single_loglikelihood(self, x):
        twovar = 2*self.variance
        return -0.5*(LOGPI + log(twovar)) - (x-self.mean)**2 / twovar

    def loglikelihoods(self, x, out=None):
        if out is None: out = np.ndarray((len(x),))
        twovar = 2*self.variance
        mu = self.mean
        c = -0.5*(LOGPI + log(twovar))
        f = lambda y: c - (y-mu)**2/twovar
        nf = np.frompyfunc(f, 1, 1)  # will not work if x has 2+ dimensions.
        nf(x,out)
        return out
    
    def estimate_parameters(self, x, weights):
        s = sum(weights)
        mu = np.inner(x,weights) / s
        var = np.inner((x-mu)**2, weights) / s
        self.mean = mu
        self.variance = var

    def initialize_parameters(self, x, weights=None, seed=None):
        if weights is None: weights = np.ones(len(x))
        self.estimate_parameters(x, weights)

#################################################
        

class BivariateNormalDistribution(Distribution):
    """Two-dimensional Gaussian with arbitrary covariance"""

    def __init__(self, mean=np.zeros(2), var=np.array([1.,1.]), cor=0.0):
        """create a new 2-dimensional normal distribution
        with given mean (2-vector), variances and correlation.
        """
        self.mean = np.array(mean)
        self.var  = np.array(var)
        self.cor  = cor

    def __repr__(self):
        return "{}(mean={}, varcor={})".format(
            self.__class__.__name__, self.mean, self.varcor)

    def single_loglikelihood(self, x):
        mu0, mu1 = self.mean
        var0, var1 = self.var
        rho = self.cor
        dx, dy = x[0]-mu0, x[1]-mu1
        f  = -LOGTWOPI - 0.5*(log(var0)+log(var1)+log1p(-rho**2))
        f -= 0.5*( dx**2/var0 + dy**2/var1 - 2*rho*dx*dy/sqrt(var0*var1) ) / (1-rho**2)
        return f
    
    def loglikelihoods(self, x, out=None):
        if out is None: out = np.ndarray((len(x),))
        varx, vary = self.var  # variances in x and y
        rho = self.cor  # correlation
        cov = rho*sqrt(varx*vary)  # covariance
        logdet = log(varx)+log(vary)+log1p(-rho**2)
        c = -LOGTWOPI - 0.5*logdet
        inv = exp(-logdet) * np.array([[var2, -cov],[-cov,var1]])
        d = x - self.mean
        for (i,dd) in enumerate(d):
            out[i] = c - 0.5* dd.dot(inv).dot(dd)
        return out

    def estimate_parameters_with_bounds(self, x, weights, minvar=0, maxcor=0.999):
        n = len(x)  # number of datapoints
        s = sum(weights)
        mu = sum(weights.reshape((n,1))*x) / s
        dx = x[:,0]-mu[0]
        dy = x[:,1]-mu[1]
        varx = np.inner(dx**2, weights) / s
        vary = np.inner(dy**2, weights) / s
        cov  = np.inner(dx*dy, weights) / s
        if varx<minvar: varx=minvar
        if vary<minvar: vary=minvar
        cor = cov/sqrt(varx*vary)
        if abs(cor)>maxcor: cor = math.copysign(maxcor,cor)
        self.cor = cor
        self.var = (varx,vary)
        self.mean = mu

    def estimate_parameters(self, x, weights):
        estimate_parameters_with_bounds(self, x, weights)


#################################################

