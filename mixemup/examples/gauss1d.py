
# pymixem example with Gauss (1D)
import numpy as np
from   numpy import random
from   pymixem import em
from   pymixem.distributions import UnivariateNormalDistribution



def points(n):
    u = random.uniform(size=n)
    one = sum((u<0.4))
    two = sum((0.4<=u) & (u<0.7))
    three = sum((0.7<=u))
    x = random.normal(loc=0.0,scale=1.0, size=(one,))
    y = random.normal(loc=2.0,scale=2.0, size=(two,))
    z = random.normal(loc=-1.0,scale=1.5, size=(three,))
    return np.concatenate((x,y,z))


def main(n, seed=17):
    random.seed(seed)
    x = points(n)
    data = em.Data(x)
    c = [UnivariateNormalDistribution(mu,1.0) for mu in range(3)]
    I = em.Instance(data, c)
    result=I.run_once()

    print("Likelihood per data point:", result['LL']/I.size)
    print("Component weights:", result['weights'])
    for (j,c) in enumerate(I.components):
        print("Component",j,":",c)
    print()


main(n=10000,seed=None)
