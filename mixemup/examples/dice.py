
# EM example: loaded dice

import collections
import numpy as np
from pymixem import em
from pymixem.distributions import FiniteDistribution, FiniteUniformDistribution
import logging

# Get logger and set level
logger = logging.getLogger("pymixem")
logger.addHandler(logging.StreamHandler())
logger.setLevel(logging.CRITICAL)  # or DEBUG / CRITICAL


def runEM(data, startweights=None, true_logp=None):
    # Create an EM instance and run EM once.
    values = frozenset(data.val)
    fair   = FiniteUniformDistribution(values)
    if true_logp is None:
        loaded = FiniteDistribution(values)
        loaded.initialize_parameters(data.val,data.hist)
    else:
        loaded = FiniteDistribution(true_logp, logarithmic=True)
    initialize_weights = lambda: startweights if startweights is not None else None
    I = em.Instance(data, (loaded,fair), initialize_weights = initialize_weights)
    result = I.run_once()
    print("Result for starting weights ",startweights,"and usetrue=",(true_logp is not None),":")
    print("Likelihood per data point:", result['LL']/I.size)
    print("Component weights:", result['weights'])
    for (j,c) in enumerate(I.components):
        print("Component",j,":",c)
    print()

# 1000 throws from a mixture of two dice: one fair, one loaded
eyes = np.array(
    (5, 5, 6, 6, 1, 3, 3, 4, 4, 1, 5, 6, 2, 4, 5, 3, 2, 2, 4, 6, 3, 6, 6, 4, 3, 6, 6, 2, 6, 6, 2, 6, 3, 2, 6, 5, 6, 3, 3, 1, 5, 1, 5, 2, 6, 6, 2, 1, 2, 4, 6, 4, 4, 4, 1, 6, 3, 5, 5, 1, 5, 5, 5, 2, 3, 1, 6, 2, 4, 3, 1, 4, 3, 2, 5, 6, 1, 4, 2, 6, 4, 4, 3, 1, 4, 1, 3, 3, 4, 6, 1, 6, 3, 1, 6, 5, 1, 5, 6, 6, 3, 1, 1, 1, 3, 6, 1, 3, 1, 2, 5, 6, 5, 1, 4, 3, 5, 5, 4, 6, 5, 2, 3, 5, 4, 4, 4, 6, 5, 4, 5, 5, 5, 4, 3, 4, 3, 5, 1, 6, 4, 3, 4, 1, 4, 6, 5, 5, 1, 6, 6, 3, 6, 1, 6, 1, 3, 3, 3, 4, 6, 6, 4, 4, 6, 2, 4, 3, 6, 2, 4, 5, 1, 2, 1, 5, 5, 3, 4, 3, 4, 3, 6, 2, 1, 3, 1, 2, 1, 3, 4, 4, 2, 6, 1, 3, 5, 5, 5, 6, 4, 1, 1, 1, 1, 1, 3, 2, 4, 3, 4, 6, 6, 2, 3, 6, 1, 4, 4, 3, 5, 6, 6, 6, 1, 2, 2, 2, 4, 5, 4, 1, 4, 2, 5, 2, 1, 4, 3, 4, 2, 5, 2, 2, 6, 2, 6, 5, 5, 2, 6, 5, 6, 3, 5, 5, 4, 6, 1, 5, 6, 1, 1, 2, 2, 6, 4, 1, 2, 5, 3, 2, 5, 1, 5, 3, 3, 4, 5, 6, 1, 1, 3, 5, 4, 2, 3, 4, 4, 3, 5, 3, 2, 4, 1, 1, 6, 4, 3, 2, 6, 1, 5, 4, 1, 5, 2, 5, 4, 5, 4, 1, 1, 2, 1, 1, 3, 3, 6, 2, 5, 5, 4, 2, 3, 5, 1, 1, 6, 5, 2, 1, 1, 2, 5, 2, 4, 6, 1, 5, 6, 3, 3, 4, 6, 3, 3, 5, 5, 3, 3, 6, 5, 4, 2, 5, 5, 6, 4, 2, 1, 5, 5, 2, 1, 1, 3, 5, 4, 5, 6, 1, 4, 3, 4, 3, 6, 5, 4, 4, 4, 1, 5, 2, 4, 3, 2, 2, 6, 3, 4, 1, 3, 6, 4, 5, 6, 5, 1, 2, 4, 3, 5, 1, 6, 1, 3, 5, 4, 1, 1, 2, 2, 4, 6, 2, 1, 5, 5, 6, 6, 6, 3, 2, 3, 4, 6, 4, 4, 3, 3, 2, 1, 6, 2, 5, 2, 6, 1, 2, 2, 1, 3, 4, 1, 6, 6, 3, 4, 6, 6, 2, 4, 1, 6, 6, 1, 5, 5, 1, 6, 6, 2, 2, 4, 1, 5, 5, 2, 4, 1, 1, 4, 6, 4, 3, 4, 6, 5, 6, 4, 2, 5, 6, 6, 5, 2, 2, 4, 4, 2, 4, 5, 2, 4, 5, 3, 6, 4, 4, 6, 6, 4, 4, 4, 5, 4, 5, 2, 2, 5, 5, 4, 3, 5, 3, 5, 6, 5, 5, 6, 2, 4, 3, 4, 5, 6, 5, 1, 1, 2, 6, 3, 6, 3, 2, 4, 4, 2, 6, 2, 1, 2, 1, 3, 2, 3, 6, 3, 6, 3, 6, 4, 4, 5, 5, 1, 3, 2, 5, 3, 5, 1, 2, 5, 5, 6, 2, 5, 5, 4, 2, 4, 1, 2, 5, 3, 6, 6, 2, 3, 6, 2, 6, 5, 3, 6, 5, 6, 6, 6, 5, 3, 3, 3, 6, 5, 3, 6, 2, 1, 2, 2, 1, 1, 6, 6, 2, 1, 5, 1, 3, 6, 2, 4, 3, 6, 6, 5, 6, 4, 5, 2, 4, 5, 3, 4, 2, 2, 5, 4, 5, 6, 1, 3, 4, 3, 4, 6, 4, 2, 2, 5, 5, 6, 6, 5, 2, 3, 5, 3, 3, 1, 4, 4, 3, 4, 2, 5, 5, 5, 1, 4, 6, 3, 5, 6, 3, 6, 3, 4, 5, 5, 1, 5, 6, 4, 4, 1, 4, 4, 1, 6, 5, 6, 4, 2, 3, 2, 3, 4, 3, 3, 2, 4, 5, 3, 5, 1, 5, 5, 6, 6, 6, 2, 1, 1, 2, 3, 3, 1, 4, 2, 1, 3, 5, 4, 4, 2, 5, 2, 4, 4, 6, 6, 5, 3, 4, 2, 4, 3, 2, 6, 2, 1, 1, 4, 2, 1, 2, 6, 1, 5, 5, 6, 1, 1, 6, 2, 5, 2, 5, 6, 5, 1, 4, 4, 2, 1, 2, 5, 4, 4, 2, 5, 6, 4, 1, 1, 3, 4, 2, 5, 4, 1, 1, 4, 3, 1, 5, 3, 6, 4, 4, 6, 5, 1, 5, 6, 1, 3, 3, 3, 2, 1, 1, 5, 2, 2, 2, 1, 1, 6, 4, 4, 3, 4, 6, 3, 3, 2, 1, 3, 3, 6, 6, 1, 3, 2, 6, 4, 5, 4, 2, 5, 5, 4, 4, 5, 6, 3, 3, 2, 3, 4, 5, 6, 4, 4, 4, 1, 2, 3, 3, 5, 1, 5, 1, 6, 4, 6, 4, 6, 5, 5, 3, 2, 4, 1, 3, 2, 5, 4, 6, 5, 2, 4, 3, 3, 6, 2, 6, 1, 6, 2, 5, 6, 5, 4, 2, 3, 1, 3, 6, 5, 5, 5, 6, 6, 2, 5, 1, 2, 4, 6, 1, 4, 2, 3, 3, 2, 3, 5, 6, 6, 4, 2, 2, 4, 1, 6, 4, 6, 1, 5, 2, 3, 2, 3, 2, 6, 1, 3, 1, 4, 6, 4, 2, 4, 6, 4, 5, 4, 4, 4, 4, 2, 3, 4, 6, 3, 6, 1, 3, 5, 2, 1, 2, 1, 2, 2, 6, 3, 1, 5, 4, 5, 2, 5, 6, 4, 3, 2, 4, 5, 6, 6, 1, 3, 2, 1, 6, 6, 4, 6, 3, 1, 3, 2, 2, 4, 3, 4, 2, 5, 3, 2, 6, 3, 1, 1, 4, 6, 5, 2, 4, 4, 3, 6, 3)
    )
data = em.Data(eyes)

# We cheat and know the loaded distribution.
true_loaded = (0.05,0.10,0.15,0.20,0.25,0.25)
true_logp   = dict(((i,np.log(v)) for (i,v) in zip(range(1,7),true_loaded)))

# We run the data through EM with different component weights,
# starting once with a guessed distribution; then with the true distribution.
startweights = [(0.2,0.8)]
# was: startweights = [ (0.2,0.8), (0.5,0.5), (0.7,0.3) ]
# was: startweights = (np.array((x, 1-x)) for x in np.arange(0.10,0.96,0.10))
for w in startweights:
    runEM(data, w)
    runEM(data, w, true_logp)
    

# Now we use the histogram form of the data;
# first we count how many times each value appears
# and create two lists.
hist = collections.Counter(eyes)
vals = sorted(hist.keys())
hist = list(hist[v] for v in vals)
hdata = em.Data(vals,hist)
for w in startweights:
    runEM(hdata, w)
    runEM(hdata, w, true_logp)
    
