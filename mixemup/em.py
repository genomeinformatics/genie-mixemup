# em.py
"""Low-level EM routines using numpy's ndarray"""
__author__ = "Sven Rahmann"

# Check for Python 3
from sys import version_info
if version_info.major<3: raise ImportError("Python 3 required.")

# Imports
import numpy as np
from   numpy import sum, exp, log
from   . import utils

##########################################################################
import logging
pymixemlogger = logging.getLogger('pymixem')
pymixemlogger.addHandler(logging.NullHandler())


##########################################################################

class Data:
    """This class describes data samples.
    If x is of type Data, then
    x.val  contains the values,
    x.hist contains multiplicites,
    x.n    contains the sum of the multiplicties."""
    
    def __init__(self, val=None, hist=None):
        """create new data vector with 'val'ues and multiplicities ('hist'ogram).
        Both 'val' and 'hist' must be iterable, and will be converted to an ndarray.
        Best practices:
        - Provide 'val' as a numpy array, such that each row is a data point.
        - Provide 'hist' as a one-dimensional numpy integer vector.
        If 'hist' is given and 'val' is None, values are taken as range(len(hist)).
        If 'val' is given and 'hist' is None, the multiplicities are 1 for each value.
        """
        if val==None and hist==None:
            raise ValueError("Either val or hist must be given.")
        if val!=None:
            self.val = np.array(val, copy=False)
        else:
            self.val = np.array(range(len(hist)))
        if hist!=None:
            self.hist = np.array(hist, copy=False)
        else:
            self.hist = np.ones(len(val))
        if len(self.val) != len(self.hist):
            raise ValueError("val and hist differ in length")
        self.n = sum(self.hist)

    def __len__(self):
        """returns the number of separate data points"""
        return len(self.val)

    def __iter__(self):
        """for each datapoint, returns the pair (value, multiplicity)"""
        for (x,n) in zip(self.val, self.hist):
            yield (x,n)

    def __repr__(self):
        return "{}({},{})".format(self.__class__.__name__, val, hist)



class Instance:
    """An Instance holds all information of an instance of an EM computation."""

    def __init__(self, x, components,
                 initialize_weights=None, estimate_weights=None):
        """create a new EM instance with samples x (of type 'Data'),
        and an iterable of components (objects of type 'distributions.Distribution').
        The Distributions must have been initialized with reasonable parameters.
        """
        self.x = x
        self.components = tuple(components)
        if initialize_weights is None:
            self.initialize_weights = utils.func_uniform(len(components))
        else:
            self.initialize_weights = initialize_weights
        if estimate_weights is None:
            self.estimate_weights = estimate_weights_ML
        else:
            self.estimate_weights = estimate_weights
        self.size = self.x.n


    def run_once(self, iterations=100, LLtolerance=1E-10):
        """run EM once on this instance."""
        N, C  =  len(self.x), len(self.components)
        xx, xh = self.x.val, self.x.hist  # x-values, multiplicities

        # Initialize weights
        weights = self.initialize_weights()

        # Initialize N*C membership matrix
        # one row for each x, one column for each component.
        Z = np.zeros((N,C), order="F") # TODO: order = "F" to access a column at once?
        totalLL = np.NINF

        # Iterate
        t = 0  # iteration number
        LLs = np.zeros(N) # total log-likelihood value for each data point
        loglik = tuple(c.loglikelihoods for c in self.components)
        
        while True:
            # E-step:
            # 1. Compute total log-likelihood under current parameters
            # 2. Compute expected assignments Z[i,j] of data points to components
            oldLL = totalLL
            for j in range(C):
                Z[:,j] = loglik[j](xx, Z[:,j])  # Fill Z[:,j] with log-likelihoods
            #print(Z)  # 
            Z += log(weights)  # add weight vector to each row
            np.logaddexp.reduce(Z, 1, out=LLs)  # LLs = total log-likelihood in each row
            # transform Z to probability matrix: Z = exp(Z - LLs)
            Z -= LLs.reshape(N,1)
            np.exp(Z, Z)  # overwrite Z with its exponentials

            # Check totalLL for convergence and stop if converged or out of time
            totalLL = np.inner(LLs, xh)                
            deltaLL = (totalLL-oldLL)/abs(oldLL)
            pymixemlogger.debug("Iteration {0:3d}: LL={1:g} (rel.impr.={2:g})".format(t,totalLL,deltaLL))
            if deltaLL <= LLtolerance: break
            t=t+1
            if t>iterations: break
            
            # M-Step: Compute new parameters
            weights = self.estimate_weights(Z, xh)
            for (j,c) in enumerate(self.components):
                c.estimate_parameters(xx, xh*Z[:,j])
            pymixemlogger.debug(
                "Iteration {0:3d}:\n  weights = {1}\n  components = {2}"\
                .format(t,weights,[str(c) for c in self.components]))

        return dict(LL=totalLL, weights=weights, iterations=t, Z=Z)

    def run(self):
        pass
        

def estimate_weights_ML(Z, multiplicities):
    w = np.inner(np.transpose(Z),multiplicities)
    w /= sum(w)
    return w

    
