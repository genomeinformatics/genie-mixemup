import numpy as np

def func_uniform(C):
    """returns the function that returns numpy array [1/C, ..., 1/C] of length C."""
    return lambda: np.ones(C)/C

